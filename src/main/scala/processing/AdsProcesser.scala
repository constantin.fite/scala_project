package processing

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.types.{StringType, StructType}

object AdsProcesser {

  def run(spark: SparkSession): Unit = {

    val rawSchema = new StructType()
      .add("os", StringType)
      .add("city", StringType)

    import spark.implicits._

    val path = "/src/main/resources/tbm-ads-data.json"

    val schema = StructType(Seq(
      StructField("network", StringType),
      StructField("appOrsite", StringType),
      StructField("city", StringType),
      StructField("timestamp", DoubleType),
      StructField("label", BooleanType),
      StructField("os", StringType),
      StructField("exchange", StringType),
      StructField("bidfloor", DoubleType),
      StructField("publisher", StringType),
      StructField("media", StringType),
      StructField("user", StringType),
      StructField("impid", StringType),
      StructField("interests", StringType),
      StructField("size", ArrayType(LongType)),
      StructField("type", StringType)



    val df  = {spark.read.schema(schema)json(path)}

    val dataset = df.withColumn("id", monotonically_increasing_id)
      .withColumn("os", lower(col("os")))
      .as[RawDataframe]

    spark.stop()

  }



  def createEnrichedataset[T](dataset: Dataset[T], spark: SparkSession): DataFrame = {
    ???
  }

}
