package processing.models

case class RawDataframe(
                         os: Option[String],
                         city: Option[String],
                         appOrsite: Option[String],
                         bidfloor: Option[Double],
                         exchange: Option[String],
                         impid: Option[String],
                         interests: Option[String],
                         media: Option[String],
                         label: Boolean,
                         network: Option[String],
                         publisher: Option[String],
                         size: Option[Array[BigInt]],
                         timestamp: Double,
                         user: Option[String],
                        type : Option[String],
                        id: String

)